package main

import "testing"

const username = "user"
const password = "pass"

func Test_givenAnyCredentials_whenLogin_thenReturnSuccessResult(t *testing.T) {
	// given
	svc := NewAuthenticationService()
	var credentials = Credentials{
		Username: username,
		Password: password,
	}

	// when
	result := svc.Login(credentials)

	// then
	if result != SUCCESS {
		t.Fail()
	}
}
