package main

import "fmt"

func authenticateCustomer(username string, password string) {
	svc := NewAuthenticationService()

	var credentials = Credentials{
		Username: username,
		Password: password,
	}
	result := svc.Login(credentials)

	if result == SUCCESS {
		fmt.Println("Authentication successful!")
	} else {
		panic("Auth failed!")
	}
}
