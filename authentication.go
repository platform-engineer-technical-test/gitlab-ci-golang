package main

type Credentials struct {
	Username string
	Password string
}

type AuthenticationResult int

const (
	SUCCESS AuthenticationResult = 0
	FAILURE AuthenticationResult = 1
)

type AuthenticationService struct {
}

func NewAuthenticationService() *AuthenticationService {
	return &AuthenticationService{}
}

func (a *AuthenticationService) Login(credentials Credentials) AuthenticationResult {
	// do business logic here
	return SUCCESS
}
