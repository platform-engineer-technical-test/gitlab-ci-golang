# gitlab-ci-golang

Dummy project to test your GitLab-CI troubleshooting skills with a Go project!

## Requirements

* Go 1.19
* A working *nix terminal

## Setup

### Building and running the code locally

```bash
go build -o pgk/
./pkg/gitlab-ci-golang
```

### Running unit tests locally

```bash
go test ./...
```
